
#include <catch.hpp>

#include "sti/sti.hpp"

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <charconv>

namespace {

void print_duration(const char* what, std::chrono::nanoseconds dur, int integer) {
		std::cout << what << " in " << std::setw(6)
			<< std::chrono::duration_cast<std::chrono::microseconds>(dur).count()
			<< "µs : " << integer << "\n";
}

void stoi_times(const std::string& str, unsigned n) {
	std::cout << n << " * \"" << str << "\":\n";
	{
		auto integer = int{};
		auto begin = std::chrono::steady_clock::now();
		for(auto i = 0u; i < n; ++i) {
			integer ^= sti::str_to<int>(str);
		}
		auto end = std::chrono::steady_clock::now();
		print_duration("str_to<int>    ", end-begin, integer);
	}
	{
		auto integer = int{};
		auto temp = int{};
		auto begin = std::chrono::steady_clock::now();
		for(auto i = 0u; i < n; ++i) {
			std::from_chars(str.data(), str.data() + str.size(), temp);
			integer ^= temp;
		}
		auto end = std::chrono::steady_clock::now();
		print_duration("std::from_chars", end-begin, integer);
	}
	{
		auto integer = int{};
		auto begin = std::chrono::steady_clock::now();
		for(auto i = 0u; i < n; ++i) {
			integer ^= std::stoi(str);
		}
		auto end = std::chrono::steady_clock::now();
		print_duration("std::stoi      ", end-begin, integer);
	}
	{
		auto integer = int{};
		auto begin = std::chrono::steady_clock::now();
		for(auto i = 0u; i < n; ++i) {
			integer ^= std::atoi(str.c_str());
		}
		auto end = std::chrono::steady_clock::now();
		print_duration("std::atoi      ", end-begin, integer);
	}
	std::cout << '\n';
}
} // namespace

TEST_CASE("to_int-performance", "[.performance]") {
	stoi_times("1", 1000000);
	stoi_times("10", 1000000);
	stoi_times("-10", 1000000);
	stoi_times("1000", 100000);
	stoi_times("10000", 1000000);
	stoi_times("100000", 10000000);
	stoi_times("1000000", 10000000);
	stoi_times("-1000000", 10000000);
}
